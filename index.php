<?php

require_once "controllers/plantilla_controlador.php";
require_once "controllers/usuarios_controlador.php";
require_once "controllers/productos_controlador.php";
require_once "controllers/categorias_controlador.php";
require_once "controllers/facultades_controlador.php";
require_once "controllers/salidas_controlador.php";

require_once "models/usuarios_modelo.php";
require_once "models/productos_modelo.php";
require_once "models/categorias_modelo.php";
require_once "models/facultades_modelo.php";
require_once "models/salidas_modelo.php";

$plantilla = new ControladorPlantilla();
$plantilla -> ctrPlantilla();