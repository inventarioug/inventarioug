<?php

class ControladorUsuario{
    static public function ctrIngresoUsuario(){
        if(isset($_POST["ingUsuario"])){
            if(preg_match('/^[a-zA-Z0-9]+$/',$_POST["ingUsuario"])
            && preg_match('/^[a-zA-Z0-9]+$/',$_POST["ingPassword"])){
                $encriptar = crypt($_POST["ingPassword"], '$2a$07$usesomesillystringfore593SIUG./20siugBjqp8I90dH6hi$');
                //$encriptar = crypt($_POST["ingPassword"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');
                $tabla = "usuarios";    $item = "usuario";  $valor = $_POST["ingUsuario"];
                $respuesta = ModeloUsuarios::MdlMostrarUsuarios($tabla,$item,$valor);
                if($respuesta["usuario"] == $_POST["ingUsuario"])
                {   if($respuesta["password"] == $encriptar)
                    {   if($respuesta["estado"] == 1){
                                /*================================
                                        VARIABLES DE SESION
                                ================================*/
                                $_SESSION["login"] = "EXITO";
                                $_SESSION["id"] = $respuesta["id"];
                                $_SESSION["nombre"] = $respuesta["nombre"];
                                $_SESSION["usuario"] = $respuesta["usuario"];
                                $_SESSION["foto"] = $respuesta["foto"];
                                $_SESSION["perfil"] = $respuesta["perfil"];

                                /*=======================================
                                   REGISTRAR FECHA Y HORA ULTIMO LOGIN
                                =========================================*/
                                date_default_timezone_set('America/Guayaquil');
                                $fecha = date('Y-m-d');
                                $hora = date('H:i:s');
                                $fechaActual = $fecha.' '.$hora;
                                $item1 = "ultimo_login";
                                $valor1 = $fechaActual;
                                $item2 = "id";
                                $valor2 = $respuesta["id"];

                                $ultimoLogin = ModeloUsuarios::mdlActualizarUsuario($tabla, $item1, $valor1, $item2, $valor2);
                                print_r($ultimoLogin);
                                if($ultimoLogin == "ok"){
                                    echo '  <script>
                                                window.location = "inicio";
                                            </script>';
                                }
                        }   
                        else if($respuesta["estado"] == 7){ echo '<br><div class="alert alert-danger">Usuario bloqueado, contactese con el administrador.</div>';  }
                        else {  echo '<br><div class="alert alert-danger">El usuario no está activo dentro del sistema.</div>';  }
                    }
                    else{  echo '<br><div class="alert alert-danger">Contraseña incorrecta, vuelve intentarlo.</div>';}
                }
                else{  echo '<br><div class="alert alert-danger">Usuario Incorrecto, vuelve intentarlo.</div>'; }
            }
        }
    }
    
    static public function ctrCrearUsuario(){
        if(isset($_POST["nuevoUsuario"])){
            if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/',$_POST["nuevoNombre"]) && 
                preg_match('/^[a-zA-Z0-9]+$/',$_POST["nuevoUsuario"]) &&
                preg_match('/^[a-zA-Z0-9]+$/',$_POST["nuevoPassword"])){
                
                $ruta = "";
                
                if(isset($_FILES["nuevaFoto"]["tmp_name"])){
                    list($ancho, $alto) = getimagesize($_FILES["nuevaFoto"]["tmp_name"]);
                    $nuevoAncho = 500;
                    $nuevoAlto = 500;
                    $directorio = "view/img/usuarios/".$_POST["nuevoUsuario"];
                    mkdir($directorio, 0755);
                    
                    if($_FILES["nuevaFoto"]["type"] == "image/jpeg"){
                        $aleatorio = mt_rand(100, 900);
                        $ruta = "view/img/usuarios/".$_POST["nuevoUsuario"]."/".$aleatorio.".jpg";
                        $origen = imagecreatefromjpeg($_FILES["nuevaFoto"]["tmp_name"]);
                        $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);
                        imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
                        imagejpeg($destino, $ruta);
                    }
                    if($_FILES["nuevaFoto"]["type"] == "image/png"){
                        $aleatorio = mt_rand(100, 900);
                        $ruta = "view/img/usuarios/".$_POST["nuevoUsuario"]."/".$aleatorio.".png";
                        $origen = imagecreatefrompng($_FILES["nuevaFoto"]["tmp_name"]);
                        $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);
                        imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
                        imagepng($destino, $ruta);
                    }
                }
                
                $tabla = "usuarios";
                $encriptar = crypt($_POST["nuevoPassword"], '$2a$07$usesomesillystringfore593SIUG./20siugBjqp8I90dH6hi$');
                $datos = array( "nombre" => $_POST["nuevoNombre"],
                                "usuario" => $_POST["nuevoUsuario"],
                                "password" => $encriptar,
                                "perfil" => $_POST["nuevoPerfil"],
                                "foto" => $ruta);
                $respuesta = ModeloUsuarios::mdlIngresarUsuario($tabla, $datos);
                
                if($respuesta == "ok"){
                        echo'<script>
                                Swal.fire({
                                    icon: "success",
                                    title: "Excelente",
                                    text: "¡El usuario a sido guardado correctamente!",
                                    showConfirmButton: true,
                                    confirmButtonText: "Cerrar"
                                    }).then(function(result){
                                        if (result.value) {
                                        window.location = "usuarios";
                                        }
                                    })
                            </script>';
                }
                elseif ($respuesta == "error") {
                        echo'<script>
                                Swal.fire({
                                    icon: "error",
                                    title: "Ooops...",
                                    text: "¡Falló el ingreso del usuario!",
                                    showConfirmButton: true,
                                    confirmButtonText: "Cerrar"
                                    }).then(function(result){
                                        if (result.value) {
                                        window.location = "usuarios";
                                        }
                                    })
                            </script>';
                }
            }   
            else{  echo'<script>
                            Swal.fire({
                                icon: "error",
                                title: "Oops...",
                                text: "¡El nombre de usuario o la contrañesa no puede ir vacío o con caracteres especiales!",
                                showConfirmButton: true,
                                confirmButtonText: "Cerrar"
                                }).then(function(result){
                                    if (result.value) {
                                    window.location = "usuarios";
                                    }
                                })
                        </script>';
            }
        }
    }
    
    static public function ctrMostrarUsuario($item, $valor){
        $tabla = "usuarios";
        $respuesta = ModeloUsuarios::mdlMostrarUsuarios($tabla, $item, $valor);
        
        return $respuesta;
    }
    
    static public function ctrEditarUsuario(){
        if(isset($_POST["editarUsuario"])){
            if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/',$_POST["editarNombre"])){
                
                $ruta = $_POST["fotoActual"];
                
                if(isset($_FILES["editarFoto"]["tmp_name"]) && !empty($_FILES["editarFoto"]["tmp_name"])){
                    list($ancho, $alto) = getimagesize($_FILES["editarFoto"]["tmp_name"]);
                    $nuevoAncho = 500;
                    $nuevoAlto = 500;
                    $directorio = "view/img/usuarios/".$_POST["editarUsuario"];
                    
                    if(!empty($_POST["fotoActual"])){
                        unlink($_POST["fotoActual"]);
                    }   else   {
                        mkdir($directorio, 0755);
                    }
                    
                    if($_FILES["editarFoto"]["type"] == "image/jpeg"){
                        $aleatorio = mt_rand(100, 900);
                        $ruta = "view/img/usuarios/".$_POST["editarUsuario"]."/".$aleatorio.".jpg";
                        $origen = imagecreatefromjpeg($_FILES["editarFoto"]["tmp_name"]);
                        $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);
                        imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
                        imagejpeg($destino, $ruta);
                    }
                    if($_FILES["editarFoto"]["type"] == "image/png"){
                        $aleatorio = mt_rand(100, 900);
                        $ruta = "view/img/usuarios/".$_POST["editarUsuario"]."/".$aleatorio.".png";
                        $origen = imagecreatefrompng($_FILES["editarFoto"]["tmp_name"]);
                        $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);
                        imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
                        imagepng($destino, $ruta);
                    }
                }
                
                $tabla = "usuarios";
                $encriptar = crypt($_POST["nuevoPassword"], '$2a$07$usesomesillystringfore593SIUG./20siugBjqp8I90dH6hi$');
                if($_POST["editarPassword"] != ""){
                    if(preg_match('/^[a-zA-Z0-9]+$/',$_POST["editarPassword"])){
                        $encriptar = crypt($_POST["editarPassword"], '$2a$07$usesomesillystringfore593SIUG./20siugBjqp8I90dH6hi$');
                    }  
                    else {
                        echo'<script>
                            Swal.fire({
                                icon: "error",
                                title: "Oops...",
                                text: "¡La contraseña no puede ir vacía o llevar caracteres especiales!",
                                showConfirmButton: true,
                                confirmButtonText: "Cerrar"
                                }).then(function(result){
                                    if (result.value) {
                                    window.location = "usuarios";
                                    }
                                })
                        </script>';
                    }                  
                }   
                else    {
                    $encriptar = $_POST["passwordActual"];
                }
                
                $datos = array( "nombre" => $_POST["editarNombre"],
                                "usuario" => $_POST["editarUsuario"],
                                "password" => $encriptar,
                                "perfil" => $_POST["editarPerfil"],
                                "foto" => $ruta
                        );
                
                $respuesta = ModeloUsuarios::mdlEditarUsuario($tabla, $datos);
                
                if($respuesta == "ok"){
                        echo'<script>
                                Swal.fire({
                                    icon: "success",
                                    title: "Excelente",
                                    text: "¡Los cambios se guardaron correctamente!",
                                    showConfirmButton: true,
                                    confirmButtonText: "Cerrar"
                                    }).then(function(result){
                                        if (result.value) {
                                        window.location = "usuarios";
                                        }
                                    })
                            </script>';
                }   
                elseif ($respuesta == "error")  {
                        echo'<script>
                                Swal.fire({
                                    icon: "error",
                                    title: "Ooops...",
                                    text: "¡Falló el ingreso del usuario!",
                                    showConfirmButton: true,
                                    confirmButtonText: "Cerrar"
                                    }).then(function(result){
                                        if (result.value) {
                                        window.location = "usuarios";
                                        }
                                    })
                            </script>';
                }
                
            }   
            else    {
                echo'<script>
                            Swal.fire({
                                icon: "error",
                                title: "Oops...",
                                text: "¡El nombre no puede estar vacío o tener caracteres especiales!",
                                showConfirmButton: true,
                                confirmButtonText: "Cerrar"
                                }).then(function(result){
                                    if (result.value) {
                                    window.location = "usuarios";
                                    }
                                })
                        </script>';
            }
        }
    }
    
    static public function ctrBorrarUsuario(){
        if(isset($_GET["idUsuario"])){
            $tabla = "usuarios";
            $datos = $_GET["idUsuario"];
            
            if($_GET["fotoUsuario"] != ""){
                unlink($_GET["fotoUsuario"]);
                rmdir('view/img/usuarios/'.$_GET["usuario"]);
            }
            
            $respuesta = ModeloUsuarios::mdlBorrarUsuario($tabla, $datos);
            if($respuesta == "ok"){
                        echo '<script>
                                Swal.fire({
                                    icon: "success",
                                    title: "Excelente",
                                    text: "¡El usuario ha sido borrado correctamente!",
                                    showConfirmButton: true,
                                    confirmButtonText: "Cerrar"
                                    }).then(function(result){
                                        if (result.value) {
                                        window.location = "usuarios";
                                        }
                                    })
                            </script>';
                }   
                else{
                        echo '<script>
                                Swal.fire({
                                    icon: "error",
                                    title: "Ooops...",
                                    text: "¡Algo falló al intentar borrar al usuario!",
                                    showConfirmButton: true,
                                    confirmButtonText: "Cerrar"
                                    }).then(function(result){
                                        if (result.value) {
                                        window.location = "usuarios";
                                        }
                                    })
                            </script>';
                }
        }
    }
    
}