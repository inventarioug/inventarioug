  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="inicio" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a>
      </li>
    </ul>

    <!-- SEARCH FORM -->
    <!--<form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form>-->

    <!-- Right navbar links -->
    <ul class="nav navbar-nav ml-auto">
        <li class="dropdown user user-menu">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <?php 
                if($_SESSION["foto"] != ""){
                    echo '<img src="'.$_SESSION["foto"].'" class="user-image">';
                }  else {   echo '<img src="view/img/usuarios/default/default_user.png" class="user-image">';  }
            ?>
            <span class="d-none d-sm-inline-block"><?php echo $_SESSION["nombre"]; ?></span>
            </a>
            <!-- DROPDOWN TOGGLE -->
            <ul class="dropdown-menu">
                <li class="user-body">
                    <div class="center-block">
                        <a href="salir" class="btn btn-default btn-flat" style="width:200px; margin:0px 25px 0px;">Salir</a>
                    </div>
                </li>
            </ul>
        </li>
    </ul>
  </nav>