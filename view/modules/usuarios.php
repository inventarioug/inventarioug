<div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h4>Administrar usuarios</h4> 
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="inicio"><i class="fas fa-tachometer-alt"></i> Inicio</a></li>
              <li class="breadcrumb-item active">Administrar usuarios</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <section class="content">
      <div class="card">
        <div class="card-header">
          <button class="btn btn-primary btnAgregarUsuario" data-toggle="modal" data-target="#modalAgregarUsuario">Agregar Usuario</button>      
        </div>
        <div class="card-body">
          <table class="table table-bordered table-striped dt-responsive nowrap" id="tablas">
            <thead>
              <tr>
                <th style="width: 10px">#</th>
                <th>Nombre</th>
                <th>Usuario</th>
                <th>Foto</th>
                <th>Rol</th>
                <th>Estado</th>
                <th>Ultimo Login</th>
                <th>Acciones</th>
              </tr>
              </thead>
              <tbody>
                  
                  <?php
                    $count = 1;
                    $item = "";
                    $valor = "";
                    $usuarios = ControladorUsuario::ctrMostrarUsuario($item, $valor);
                    foreach ($usuarios as $key => $value){
                        if($value["estado"] == 1 || $value["estado"] == 0 || $value["estado"] == 7){
                            echo '  <tr>
                                    <td>'.$count.'</td>
                                    <td>'.$value["nombre"].'</td>
                                    <td>'.$value["usuario"].'</td>';

                            if($value["foto"] != ""){
                                echo '<td><img src="'.$value["foto"].'" class="img-thumbnail" width="40px" alt="imagen usuario"></td>';
                            }else{
                                echo '<td><img src="view/img/usuarios/default/default_user.png" class="img-thumbnail" width="40px" alt="imagen usuario"></td>';
                            }

                            echo'   <td>'.$value["perfil"].'</td>';
                            if($value["estado"] == 1){            echo '<td><button class="btn btn-success btn-xs btnActivar" idUsuario="'.$value["id"].'" estadoUsuario="0" estado="1">Activado</button></td>';   }
                            else if($value["estado"] == 0){       echo '<td><button class="btn btn-danger btn-xs btnActivar" idUsuario="'.$value["id"].'" estadoUsuario="1" estado="0">Desactivado</button></td>';   }
                            else if($value["estado"] == 7){       echo '<td><button class="btn btn-warning btn-xs btnActivar" idUsuario="'.$value["id"].'" estadoUsuario="1" estado="7">Bloqueado</button></td>';   }

                            echo'   <td>'.$value["ultimo_login"].'</td>
                                    <td>
                                      <div class="btn-group">
                                        <button class="btn btn-warning btnEditarUsuario" idUsuario="'.$value["id"].'" data-toggle="modal" data-target="#modalEditarUsuario"><i class="fa fa-cog" aria-hidden="true"></i></button>
                                        <button class="btn btn-danger btnEliminarUsuario" usuario="'.$value["usuario"].'" idUsuario="'.$value["id"].'" fotoUsuario="'.$value["foto"].'"><i class="fa fa-times"></i></button> 
                                      </div>
                                    </td>
                                  </tr>';
                            $count++;
                        }    
                    }
                  ?>
              </tbody>
          </table>
        </div>
      </div>
    </section>
  </div>

<!-- ========================================
            MODAL CREAR USUARIOS
===========================================-->

<div class="modal fade" id="modalAgregarUsuario" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <form role="form" method="post" enctype="multipart/form-data">
            <div class="modal-header" style="background: #3c8dbc; color:white ;">
              <h5 class="modal-title" id="exampleModalLabel">Agregar Usuario</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="box-body">
                  <div class="form-group"> 
                      <div class="input-group"> 
                          <div class="input-group-append">
                              <div class="input-group-text">
                                  <span class="fas fa-user"></span>
                              </div>
                          </div>  
                     <input type="text" class="form-control input-lg" name="nuevoNombre" placeholder="Ingresar nombre" required>
                    </div>
                  </div>

                  <div class="form-group"> 
                      <div class="input-group"> 
                          <div class="input-group-append">
                              <div class="input-group-text">
                                  <span class="fas fa-key"></span>
                              </div>
                          </div>  
                     <input type="text" class="form-control input-lg" name="nuevoUsuario" id="nuevoUsuario" placeholder="Ingresar usuario" required>
                    </div>
                  </div>

                  <div class="form-group"> 
                      <div class="input-group"> 
                          <div class="input-group-append">
                              <div class="input-group-text">
                                  <span class="fas fa-lock"></span>
                              </div>
                          </div>  
                          <input type="password" class="form-control input-lg" name="nuevoPassword" placeholder="Ingresar contraseña" required>
                    </div>
                  </div>

                  <div class="form-group"> 
                      <div class="input-group"> 
                          <div class="input-group-append">
                              <div class="input-group-text">
                                  <span class="fas fa-users"></span>
                              </div>
                          </div>  
                          <select name="nuevoPerfil" class="form-control input-lg">
                              <option value="Administrador">Administrador</option>
                              <option value="Especial">Especial</option>
                              <option value="Despachante">Despachador</option>
                          </select>
                    </div>
                  </div>

                  <div class="form-group"> 
                      <div class="panel">Subir foto</div>
                      <div class="custom-file">
                          <input type="file" class="custom-file-input nuevaFoto" id="nuevaFoto" name="nuevaFoto">
                          <label class="custom-file-label" for="customFile">Seleccione un archivo</label>
                      </div>
                      <small class="form-text text-muted">Peso máximo de la foto 2mb</small>
                      <img src="view/img/usuarios/default/default_user.png" class="img-thumbnail previsualizar" width="100px" alt="Imagen Usuario"/>
                  </div>

               </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
              <button type="submit" class="btn btn-primary swal">Guardar</button>
            </div>
            <?php
                $crearUsuario = new ControladorUsuario();
                $crearUsuario -> ctrCrearUsuario();
            ?>
        </form>
    </div>
  </div>
</div>

<!--========================================
            MODAL EDITAR USUARIOS
===========================================-->

<div class="modal fade" id="modalEditarUsuario" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <form role="form" method="post" enctype="multipart/form-data">
            <div class="modal-header" style="background: #3c8dbc; color:white ;">
              <h5 class="modal-title" id="exampleModalLabel">Editar Usuario</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="box-body">
                  <div class="form-group"> 
                      <div class="input-group"> 
                          <div class="input-group-append">
                              <div class="input-group-text">
                                  <span class="fas fa-user"></span>
                              </div>
                          </div>  
                     <input type="text" class="form-control input-lg" id="editarNombre" name="editarNombre" value="" required>
                    </div>
                  </div>

                  <div class="form-group"> 
                      <div class="input-group"> 
                          <div class="input-group-append">
                              <div class="input-group-text">
                                  <span class="fas fa-key"></span>
                              </div>
                          </div>  
                          <input type="text" class="form-control input-lg" id="editarUsuario" name="editarUsuario" value="" readonly="">
                    </div>
                  </div>

                  <div class="form-group"> 
                      <div class="input-group"> 
                          <div class="input-group-append">
                              <div class="input-group-text">
                                  <span class="fas fa-lock"></span>
                              </div>
                          </div>  
                          <input type="password" class="form-control input-lg" name="editarPassword" placeholder="Ingresar nueva contraseña">
                          <input type="hidden" id="passwordActual" name="passwordActual">
                    </div>
                  </div>

                  <div class="form-group"> 
                      <div class="input-group"> 
                          <div class="input-group-append">
                              <div class="input-group-text">
                                  <span class="fas fa-users"></span>
                              </div>
                          </div>  
                          <select name="editarPerfil" class="form-control input-lg">
                              <option value="" id="editarPerfil"></option>
                              <option value="Administrador">Administrador</option>
                              <option value="Especial">Especial</option>
                              <option value="Despachante">Despachador</option>
                          </select>
                    </div>
                  </div>

                  <div class="form-group"> 
                      <div class="panel">Subir foto</div>
                      <div class="custom-file">
                          <input type="file" class="custom-file-input nuevaFoto" id="editarFoto" name="editarFoto">
                          <label class="custom-file-label" for="customFile">Seleccione un archivo</label>
                      </div>
                      <small class="form-text text-muted">Peso máximo de la foto 2mb</small>
                      <img src="view/img/usuarios/default/default_user.png" class="img-thumbnail previsualizar" width="100px" alt="Imagen Usuario"/>
                      <input type="hidden" name="fotoActual" id="fotoActual">
                  </div>

               </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
              <button type="submit" class="btn btn-primary swal">Guardar</button>
            </div>       
            <?php
                $editarUsuario = new ControladorUsuario();
                $editarUsuario -> ctrEditarUsuario();
            ?>
        </form>
    </div>
  </div>
</div>

<?php
    $borrarUsuario = new ControladorUsuario();
    $borrarUsuario ->ctrBorrarUsuario();
?>