<footer class="main-footer">
    <strong>Copyright &copy; 2020 - 2021 <a href="http://ug.edu.ec" target="_blank">Universidad de Guayaquil</a> </strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Sistema de Inventarios</b> 1.0.0
    </div>
</footer>