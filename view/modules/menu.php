<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="inicio" class="brand-link">
      <img src="view/img/plantilla/LogosUG-03.png" alt="Logo UG" class="brand-image elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Inventories UG</span>
    </a>

    <div class="sidebar">
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item has-treeview">
                    <a href="inicio" class="nav-link active">
                        <i class="nav-icon fas fa-home"></i>
                        <p>
                            Inicio
                        </p>
                    </a>
                <li>
                <li class="nav-item">
                    <a href="usuarios" class="nav-link">
                        <i class="nav-icon fas fa-user"></i>
                        <p>
                            Usuarios
                            <span class="right badge badge-danger">New</span>
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="categorias" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Categorias
                            <span class="right badge badge-danger">New</span>
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="productos" class="nav-link">
                        <i class="nav-icon fab fa-product-hunt"></i>
                        <p>
                            Productos
                            <span class="right badge badge-danger">New</span>
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="facultades" class="nav-link">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                            Facultades
                            <span class="right badge badge-danger">New</span>
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview menu-close">
                    <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-list-ul"></i>
                    <p>
                        Salida de Producto
                        <i class="right fas fa-angle-left"></i>
                    </p>
                    </a>
                    <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="salidas" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Administrar Salidas</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="crear-salida" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Crear Salida </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="reportes" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Reporte de Salidas</p>
                        </a>
                    </li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</aside>