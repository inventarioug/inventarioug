<div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Pagina no Encontrada</h1> 
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="inicio"><i class="fas fa-tachometer-alt"></i> Inicio</a></li>
              <li class="breadcrumb-item active">Pagina no Encontrada</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <section class="content">
        <div class="error-page">
            <h2 class="headline text-primary">404</h2>
            <div class="error-content">
                <h3><i class="fas fa-exclamation-triangle text-warning"></i>Oops! Página no encontrada.</h3>
                <p>
                    En el menú lateral podrá encontrar las páginas disponibles.
                    Tambien puede regresar al inicio haciendo <a href="inicio">click aquí</a>
                </p>
            </div>
        </div>
    </section>
  </div>
