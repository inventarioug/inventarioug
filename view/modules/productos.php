<div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Administrar productos</h1> 
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="inicio"><i class="fas fa-tachometer-alt"></i> Inicio</a></li>
              <li class="breadcrumb-item active">Administrar Productos</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <section class="content">
      <div class="card">
        <div class="card-header">
          <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarProducto">Agregar Producto</button>      
        </div>
        <div class="card-body">
            <table class="table table-bordered table-hoverx table-striped dt-responsive nowrap dataTable_width-margin_auto" id="tablas">
                <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Imagen</th>
                        <th>Código</th>
                        <th>Descripción</th>
                        <th>Categoría</th>
                        <th>Stock</th>
                        <th>$ Compra</th>
                        <th>$ Venta</th>
                        <th>Agregado</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td><img src="view/img/productos/default/default.png" class="img-thumbnail" width="40px" alt="imagen producto"></td>
                        <td>RF81</td>
                        <td>Lorem ipsum dolor sit amet</td>
                        <td>Categoria Lorem</td>
                        <td>20</td>
                        <td>5.00</td>
                        <td>7.00</td>
                        <td>2017-12-11 12:05:32</td>
                        <td>
                          <div class="btn-group">
                            <button class="btn btn-warning"><i class="fa fa-cog" aria-hidden="true"></i></button>
                            <button class="btn btn-danger"><i class="fa fa-times"></i></button> 
                          </div>
                        </td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td><img src="view/img/productos/default/default.png" class="img-thumbnail" width="40px" alt="imagen producto"></td>
                        <td>RF81</td>
                        <td>Lorem ipsum dolor sit amet</td>
                        <td>Categoria Lorem</td>
                        <td>20</td>
                        <td>5.00</td>
                        <td>7.00</td>
                        <td>2017-12-11 12:05:32</td>
                        <td>
                          <div class="btn-group">
                            <button class="btn btn-warning"><i class="fa fa-cog" aria-hidden="true"></i></button>
                            <button class="btn btn-danger"><i class="fa fa-times"></i></button> 
                          </div>
                        </td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td><img src="view/img/productos/default/default.png" class="img-thumbnail" width="40px" alt="imagen producto"></td>
                        <td>RF81</td>
                        <td>Lorem ipsum dolor sit amet</td>
                        <td>Categoria Lorem</td>
                        <td>20</td>
                        <td>5.00</td>
                        <td>7.00</td>
                        <td>2017-12-11 12:05:32</td>
                        <td>
                          <div class="btn-group">
                            <button class="btn btn-warning"><i class="fa fa-cog" aria-hidden="true"></i></button>
                            <button class="btn btn-danger"><i class="fa fa-times"></i></button> 
                          </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </section>
  </div>

<!-- ========================================
            MODAL AGREGAR PRODUCTO
===========================================-->

<div class="modal fade" id="modalAgregarProducto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <form role="form" method="post" enctype="multipart/form-data">
            <div class="modal-header" style="background: #3c8dbc; color:white ;">
              <h5 class="modal-title" id="exampleModalLabel">Agregar Producto</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="box-body">
                    <div class="form-group"> 
                        <div class="input-group"> 
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-code"></span>
                                </div>
                            </div>  
                            <input type="text" class="form-control input-lg" name="nuevoCodigo" placeholder="Ingresar codigo" required>
                        </div>
                    </div>

                    <div class="form-group"> 
                        <div class="input-group"> 
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fab fa-product-hunt"></span>
                                </div>
                            </div>  
                       <input type="text" class="form-control input-lg" name="nuevaDescripcion" placeholder="Ingresar Descripcion" required>
                      </div>
                    </div>

                    <div class="form-group"> 
                        <div class="input-group"> 
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-th"></span>
                                </div>
                            </div>  
                            <select name="nuevaCategoria" class="form-control input-lg">
                                <option value="Taladros">Taladro</option>
                                <option value="Andamios">Andamio</option>
                                <option value="Construccion">Construccion</option>
                            </select>
                      </div>
                    </div>
                  
                    <div class="form-group"> 
                        <div class="input-group"> 
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-check"></span>
                                </div>
                            </div>  
                       <input type="number" class="form-control input-lg" name="nuevoStock" min="0" placeholder="Cantidad Disponible" required>
                      </div>
                    </div>
                  
                    <div class="form-group row"> 
                        <div class="col-6">
                            <div class="input-group"> 
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fas fa-arrow-up"></span>
                                    </div>
                                </div>  
                                <input type="number" class="form-control input-lg" name="nuevoPrecioCompra" min="0" placeholder="Precio Compra" required>
                            </div>
                        </div>
                        
                        <div class="col-6 row">
                            <div class="input-group"> 
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fas fa-arrow-down"></span>
                                    </div>
                                </div>  
                                <input type="number" class="form-control input-lg" name="nuevoPrecioVenta" min="0" placeholder="Precio Venta" required>
                            </div>
                            <br/>
                            <div class="col-sm-6">
                                <div class="form-group icheck-primary">
                                    <input type="checkbox" id="checkboxPercent" porcentaje/>
                                    <label for="checkboxPercent">
                                        Utilizar Porcentaje (%)
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-6" style="padding:2%;">
                                <div class="input-group">
                                    <input type="number" class="form-control input-lg" name="nuevoPorcentaje" value="40" required>
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span class="fas fa-percent"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                  <div class="form-group"> 
                      <div class="panel">Subir Imagen</div>
                      <div class="custom-file">
                          <input type="file" class="custom-file-input" id="nuevaImagen" name="nuevaImagen">
                          <label class="custom-file-label" for="customFile">Seleccione un archivo</label>
                      </div>
                      <small class="form-text text-muted">Peso máximo de la foto 10mb</small>
                      <img src="view/img/productos/default/default.png" class="img-thumbnail" width="100px" alt="Imagen Producto"/>
                  </div>

               </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
              <button type="button" class="btn btn-primary">Guardar</button>
            </div>
        </form>
    </div>
  </div>
</div>