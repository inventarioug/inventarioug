<div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-0">
          <div class="col-sm-6">
            <h4>Administrar categorías</h4> 
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="inicio"><i class="fas fa-tachometer-alt"></i> Inicio</a></li>
              <li class="breadcrumb-item active">Administrar categorías</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <section class="content">
      <div class="card">
        <div class="card-header">
          <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarCategoria">Agregar Categoría</button>      
        </div>
        <div class="card-body">
          <table class="table table-bordered table-striped dt-responsive nowrap" id="tablas">
            <thead>
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Categoría</th>
                  <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                  <td>1</td>
                  <td>Equipos de Computo</td>
                  <td>
                    <div class="btn-group">
                      <button class="btn btn-warning"><i class="fa fa-cog" aria-hidden="true"></i></button>
                      <button class="btn btn-danger"><i class="fa fa-times"></i></button> 
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>Equipos de Oficina</td>
                  <td>
                    <div class="btn-group">
                      <button class="btn btn-warning"><i class="fa fa-cog" aria-hidden="true"></i></button>
                      <button class="btn btn-danger"><i class="fa fa-times"></i></button> 
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>3</td>
                  <td>Equipos Electrónicos</td>
                  <td>
                    <div class="btn-group">
                      <button class="btn btn-warning"><i class="fa fa-cog" aria-hidden="true"></i></button>
                      <button class="btn btn-danger"><i class="fa fa-times"></i></button> 
                    </div>
                  </td>
                </tr>
            </tbody>
          </table>
        </div>
    </section>
  </div>

<!-- ========================================
            MODAL CATEGORIAS
===========================================-->

<div class="modal fade" id="modalAgregarCategoria" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <form role="form" method="post">
            <div class="modal-header" style="background: #3c8dbc; color:white ;">
              <h5 class="modal-title" id="exampleModalLabel">Agregar Categoría</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="box-body">
                    <div class="form-group"> 
                        <div class="input-group"> 
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-th"></span>
                                </div>
                            </div>  
                            <input type="text" class="form-control input-lg" name="nuevoCategoria" placeholder="Ingresar Categoría" required>
                        </div>
                    </div>
               </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
              <button type="button" class="btn btn-primary">Guardar</button>
            </div>
        </form>
    </div>
  </div>
</div>