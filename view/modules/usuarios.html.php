<div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Administrar usuarios</h1> 
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="inicio"><i class="fas fa-tachometer-alt"></i> Inicio</a></li>
              <li class="breadcrumb-item active">Administrar usuarios</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <section class="content">
      <div class="card">
        <div class="card-header">
          <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarUsuario">Agregar Usuario</button>      
        </div>
        <div class="card-body">
          <table class="table table-bordered table-striped dt-responsive nowrap" id="tablas">
            <thead>
              <tr>
                <th style="width: 10px">#</th>
                <th>Nombre</th>
                <th>Usuario</th>
                <th>Foto</th>
                <th>Rol</th>
                <th>Estado</th>
                <th>Ultimo Login</th>
                <th>Acciones</th>
              </tr>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>Administrador</td>
                  <td>admin</td>
                  <td><img src="view/img/usuarios/default/default_user.png" class="img-thumbnail" width="40px" alt="imagen usuario"></td>
                  <td>Administrativo</td>
                  <td><button class="btn btn-success btn-xs">Activado</button></td>
                  <td>2017-12-11 12:05:32</td>
                  <td>
                    <div class="btn-group">
                      <button class="btn btn-warning"><i class="fa fa-cog" aria-hidden="true"></i></button>
                      <button class="btn btn-danger"><i class="fa fa-times"></i></button> 
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>Operador</td>
                  <td>oper</td>
                  <td><img src="view/img/usuarios/default/default_user.png" class="img-thumbnail" width="40px" alt="imagen usuario"></td>
                  <td>Administrativo</td>
                  <td><button class="btn btn-success btn-xs">Activado</button></td>
                  <td>2017-12-11 12:05:32</td>
                  <td>
                    <div class="btn-group">
                      <button class="btn btn-warning"><i class="fa fa-cog" aria-hidden="true"></i></button>
                      <button class="btn btn-danger"><i class="fa fa-times"></i></button> 
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>Colaborador</td>
                  <td>colab</td>
                  <td><img src="view/img/usuarios/default/default_user.png" class="img-thumbnail" width="40px" alt="imagen usuario"></td>
                  <td>Administrativo</td>
                  <td><button class="btn btn-danger btn-xs">Desactivado</button></td>
                  <td>2017-12-11 12:05:32</td>
                  <td>
                    <div class="btn-group">
                      <button class="btn btn-warning"><i class="fa fa-cog" aria-hidden="true"></i></button>
                      <button class="btn btn-danger"><i class="fa fa-times"></i></button> 
                    </div>
                  </td>
                </tr>
              </tbody>
            </thead>
          </table>
        </div>
    </section>
  </div>

<!-- ========================================
            MODAL USUARIOS
===========================================-->

<div class="modal fade" id="modalAgregarUsuario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <form role="form" method="post" enctype="multipart/form-data">
            <div class="modal-header" style="background: #3c8dbc; color:white ;">
              <h5 class="modal-title" id="exampleModalLabel">Agregar Usuario</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="box-body">
                  <div class="form-group"> 
                      <div class="input-group"> 
                          <div class="input-group-append">
                              <div class="input-group-text">
                                  <span class="fas fa-user"></span>
                              </div>
                          </div>  
                     <input type="text" class="form-control input-lg" name="nuevoNombre" placeholder="Ingresar nombre" required>
                    </div>
                  </div>

                  <div class="form-group"> 
                      <div class="input-group"> 
                          <div class="input-group-append">
                              <div class="input-group-text">
                                  <span class="fas fa-key"></span>
                              </div>
                          </div>  
                     <input type="text" class="form-control input-lg" name="nuevoUsuario" placeholder="Ingresar usuario" required>
                    </div>
                  </div>

                  <div class="form-group"> 
                      <div class="input-group"> 
                          <div class="input-group-append">
                              <div class="input-group-text">
                                  <span class="fas fa-lock"></span>
                              </div>
                          </div>  
                          <input type="password" class="form-control input-lg" name="nuevoPassword" placeholder="Ingresar contraseña" required>
                    </div>
                  </div>

                  <div class="form-group"> 
                      <div class="input-group"> 
                          <div class="input-group-append">
                              <div class="input-group-text">
                                  <span class="fas fa-users"></span>
                              </div>
                          </div>  
                          <select name="nuevoPerfil" class="form-control input-lg">
                              <option value="Administrador">Administrador</option>
                              <option value="Especial">Especial</option>
                              <option value="Despachante">Despachate</option>
                          </select>
                    </div>
                  </div>

                  <div class="form-group"> 
                      <div class="panel">Subir foto</div>
                      <div class="custom-file">
                          <input type="file" class="custom-file-input" id="nuevaFoto" name="nuevaFoto">
                          <label class="custom-file-label" for="customFile">Seleccione un archivo</label>
                      </div>
                      <small class="form-text text-muted">Peso máximo de la foto 10mb</small>
                      <img src="view/img/usuarios/default/default_user.png" class="img-thumbnail" width="100px" alt="Imagen Usuario"/>
                  </div>

               </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
              <button type="button" class="btn btn-primary">Guardar</button>
            </div>
        </form>
    </div>
  </div>
</div>