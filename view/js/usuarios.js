/*====================================
       SUBIENDO FOTO DE USUARIO
 ====================================*/
$(".nuevaFoto").change(function(){
    var imagen = this.files[0];
    
    if(imagen["type"]!== "image/jpeg" && imagen["type"]!== "image/png"){
        $(".nuevaFoto").val("");
        Swal.fire({
            icon: "error",
            title: "Error al subir imagen",
            text: "¡La imagen debe ser de formato JPG o PNG!",
            showConfirmButton: true,
            confirmButtonText: "Cerrar"
        });
    }
    else if(imagen["size"] > 2000000){
        $(".nuevaFoto").val("");
        Swal.fire({
            icon: "error",
            title: "Error al subir imagen",
            text: "¡La imagen no debe pesar mas de 2MB!",
            showConfirmButton: true,
            confirmButtonText: "Cerrar"
        });
    }
    else{
        var datosImagen = new FileReader;
        datosImagen.readAsDataURL(imagen);
        
        $(datosImagen).on("load",function(event){
            var rutaImagen = event.target.result;
            $(".previsualizar").attr("src", rutaImagen);
        });
    }
});

/*====================================
            EDITAR USUARIO
 ====================================*/
$(document).on("click",".btnEditarUsuario",function(){
    var idUsuario = $(this).attr("idUsuario");
    var datos = new FormData();
    datos.append("idUsuario", idUsuario);
    
    $.ajax({
       url:"ajax/usuarios.ajax.php",
       method: "POST",
       data: datos,
       cache: false,
       contentType: false,
       processData: false,
       dataType: "json",
       success: function(respuesta){
           console.log(respuesta);
           $("#editarNombre").val(respuesta["nombre"]);
           $("#editarUsuario").val(respuesta["usuario"]);
           $("#passwordActual").val(respuesta["password"]);
           $("#editarPerfil").html(respuesta["perfil"]);
           $("#editarPerfil").val(respuesta["perfil"]);
           $("#fotoActual").val(respuesta["foto"]);
           
           if(respuesta["foto"] !== ""){
               $(".previsualizar").attr("src", respuesta["foto"]);
           } else {$(".previsualizar").attr("src", "view/img/usuarios/default/default_user.png");}
           
       }
    }); 
});

/*====================================
    FOTO DEFAULT AGREGAR USUARIOS
 ====================================*/
$(".btnAgregarUsuario").click(function(){
    $(".alert").remove();
    $(".previsualizar").attr("src", "view/img/usuarios/default/default_user.png"); 
});

/*====================================
            ACTIVAR USUARIOS
 ====================================*/
$(document).on("click",".btnActivar",function(){
    var idUsuario = $(this).attr("idUsuario");
    var estadoUsuario = $(this).attr("estadoUsuario");
    var estado = $(this).attr("estado");
    
    var datos = new FormData();
    datos.append("activarId", idUsuario);
    datos.append("activarUsuario", estadoUsuario);
    $.ajax({
       url:"ajax/usuarios.ajax.php",
       method:"POST",
       data:datos,
       cache:false,
       contentType:false,
       processData:false,
       success:function(respuesta){ 
           if(window.matchMedia("(max-width:767px)").matches){
               Swal.fire({
                    icon: "success",
                    title: "Excelente",
                    text: "¡El usuario se actualizó correctamente!",
                    showConfirmButton: true,
                    confirmButtonText: "Cerrar"
                }).then(function(result){
                    if (result.value) {
                        window.location = "usuarios";
                    }
                });
           }
       }
    });
    
    //Estado Activado
    if(estado == 1){
           $(this).removeClass('btn-success');
           $(this).addClass('btn-danger');
           $(this).html('Desactivado');
           $(this).attr('estadoUsuario',1);
           $(this).attr('estado',0);
    }//Estado Bloqueado
    else if(estado == 7){
           $(this).removeClass('btn-danger');
           $(this).removeClass('btn-warning');
           $(this).addClass('btn-success');
           $(this).html('Activado');
           $(this).attr('estadoUsuario',0);
           $(this).attr('estado',1);
    }//Estado Desactivado
    else if(estado == 0){
           $(this).removeClass('btn-danger');
           $(this).removeClass('btn-warning');
           $(this).addClass('btn-success');
           $(this).html('Activado');
           $(this).attr('estadoUsuario',0);
           $(this).attr('estado',1);
    }
});

/*====================================
    VALIDAR USUARIOS REPETIDOS
 ====================================*/
$('#nuevoUsuario').change(function(){
   $(".alert").remove();
   var usuario = $(this).val();
   var datos = new FormData();
   datos.append("validarUsuario", usuario);
   $.ajax({
       url:"ajax/usuarios.ajax.php",
       method:"POST",
       data:datos,
       cache:false,
       contentType:false,
       processData:false,
       dataType: "json",
       success:function(respuesta){
           if(respuesta){
               $('#nuevoUsuario').parent().after('<div class="alert alert-warning">Este usuario no disponible.</div>');
               $('#nuevoUsuario').val("");
           }
           else{
               $('#nuevoUsuario').parent().after('<div class="alert alert-success">Usuario disponible.</div>');
           }
       }
   });   
});

/*====================================
        ELIMINAR USUARIOS
 ====================================*/
$(document).on("click",".btnEliminarUsuario",function(){
    var idUsuario = $(this).attr("idUsuario");
    var fotoUsuario = $(this).attr("fotoUsuario");
    var usuario = $(this).attr("usuario");
    
    Swal.fire({
        icon: "warning",
        title: "¿Está seguro que desea borrar este usuario?",
        text: "¡Si no lo está puede cancelar la acción!",
        showCancelButton: true,
        confirmButtonColor: '#3028d6',
        cancelButtonColor: '#d33',
        cancelButtonText: "Cancelar",
        confirmButtonText: "Sí, borrar usuario!"
    }).then(function(result){
       if(result.value){
           window.location = "index.php?ruta=usuarios&idUsuario="+idUsuario+"&usuario="+usuario+"&fotoUsuario="+fotoUsuario;
       } 
    });  
});