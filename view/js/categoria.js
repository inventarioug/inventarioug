/*========================================
            EDITAR CATEGORIA
===========================================*/
$(".tablas").on("click", ".btnEditarCategoria", function(){
	var idCategoria = $(this).attr("idCategoria");
	var datos = new FormData();
	datos.append("idCategoria", idCategoria);
	$.ajax({
            url: "ajax/categorias.ajax.php",
            method: "POST",
            data: datos,
            cache: false,
            contentType: false,
            processData: false,
            dataType:"json",
            success: function(respuesta){
                    $("#editarCategoria").val(respuesta["categoria"]);
                    $("#idCategoria").val(respuesta["id"]);
     	}
	});
});

/*=============================================
            ELIMINAR CATEGORIA
=============================================*/
$(".tablas").on("click", ".btnEliminarCategoria", function(){
	 var idCategoria = $(this).attr("idCategoria");
	 Swal.fire({
            icon: "warning",
            title: "¿Está seguro que desea borrar esta categoria?",
            text: "¡Si no lo está puede cancelar la acción!",
            showCancelButton: true,
            confirmButtonColor: '#3028d6',
            cancelButtonColor: '#d33',
            cancelButtonText: "Cancelar",
            confirmButtonText: "Sí, borrar categoria!"
        }).then(function(result){
           if(result.value){
	 	window.location = "index.php?ruta=categorias&idCategoria="+idCategoria;
            }
	});
});