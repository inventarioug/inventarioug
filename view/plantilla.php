<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link  rel="icon"   href="public/img/favicon.png" type="image/png" />
  <title>Inventarios UG</title>
  <!-- Tell the browser to be responsive to screen width -->
    
  <!--=================================
  PLUGINS CSS
  ==================================-->
  <!-- Font Awesome -->
  <link rel="stylesheet" href="view/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="view/dist/css/adminlte.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="view/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="view/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="view/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="view/plugins/icheck-bootstrap/icheck-bootstrap.css">
  <!-- SweetAlert v2 -->
  <link rel="stylesheet" href="view/plugins/sweetalert2/sweetalert2.css"/>
  <!-- SweetAlert v2 -->
  <script src="view/plugins/sweetalert2/sweetalert2.all.js"></script>
  <!-- By default SweetAlert2 doesn't support IE. To enable IE 11 support, include Promise polyfill:-->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
  
    <style>
        table.dataTable.dataTable_width-margin_auto {
            width: auto;
            margin: auto;
        }
    </style>
</head>
 <!--=================================
  CUERPO DOCUMENTO
  ==================================-->
<body class="hold-transition sidebar-collapse sidebar-mini login-page">
    <?php
      if(isset($_SESSION["login"]) && $_SESSION["login"] == "EXITO")
      {
        echo '<div class="wrapper">';
        include "modules/cabezote.php";
        include "modules/menu.php";

        if(isset($_GET["ruta"])){
            if($_GET["ruta"] == "inicio"       ||
              $_GET["ruta"] ==  "usuarios"     ||
              $_GET["ruta"] ==  "categorias"   ||
              $_GET["ruta"] ==  "productos"    ||
              $_GET["ruta"] ==  "facultades"   ||
              $_GET["ruta"] ==  "salidas"      ||
              $_GET["ruta"] ==  "crear-salida" ||
              $_GET["ruta"] ==  "reportes"     ||
              $_GET["ruta"] ==  "salir"){
              include "modules/".$_GET["ruta"].".php";
            }
            else
            { include "modules/404.php";  }
        } 
        else  { include "modules/inicio.php"; }
        
        include "modules/footer.php";
        echo '</div>';
      }
      else {include "modules/login.php";}
    ?>
  <!--=================================
  PLUGINS JS
  ==================================-->
    <!-- jQuery -->
    <script src="view/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="view/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- FastClick -->
    <script src="view/plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="view/dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="view/dist/js/demo.js"></script>
    <!-- Personalize JS -->
    <script src="view/js/plantilla.js"></script>
    <script src="view/js/usuarios.js"></script>
    <script src="view/js/categoria.js"></script>
    <!-- DataTables -->
    <script src="view/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="view/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="view/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="view/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
    

  
  <script>
    $(".custom-file-input").on("change", function() {
      var fileName = $(this).val().split("\\").pop();
      $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
  </script>

</body>
</html>
