<?php
require_once "../controllers/usuarios_controlador.php";
require_once "../models/usuarios_modelo.php";

class AjaxUsuario{ 

    public $idUsuario;   
    public function ajaxEditarUsuario(){
        $item = "id";
        $valor = $this->idUsuario;
        $respuesta = ControladorUsuario::ctrMostrarUsuario($item, $valor);
        echo json_encode($respuesta);
    }

    public $activarId;
    public $activarUsuario;    
    public function ajaxActivarUsuario(){
        $tabla = "usuarios";
        $item1 = "estado";
        $valor1 =  $this->activarUsuario;
        $item2 = "id";
        $valor2 = $this->activarId;
        $respuesta = ModeloUsuarios::mdlActualizarUsuario($tabla, $item1, $valor1, $item2, $valor2);
    }
    
    public $validarUsuario;
    public function ajaxValidarUsuario(){
        $item = "usuario";
        $valor = $this->validarUsuario;
        $respuesta = ControladorUsuario::ctrMostrarUsuario($item, $valor);
        if($respuesta["estado"] == 10){echo json_encode("");}
        else {echo json_encode($respuesta);}
        
    }
    
}
/*====================================
        EDITAR USUARIO
 ====================================*/
if (isset($_POST["idUsuario"])){
    $editar = new AjaxUsuario();
    $editar -> idUsuario = $_POST["idUsuario"];
    $editar ->ajaxEditarUsuario();
}
    
/*====================================
        ACTIVAR USUARIO
====================================*/
if (isset($_POST["activarUsuario"])){
    $editar = new AjaxUsuario();
    $editar -> activarUsuario = $_POST["activarUsuario"];
    $editar -> activarId = $_POST["activarId"];
    $editar ->ajaxActivarUsuario();
}

/*====================================
    VAlIDAR USUARIO REPETIDO
====================================*/
if (isset($_POST["validarUsuario"])){
    $valUsuario = new AjaxUsuario();
    $valUsuario -> validarUsuario = $_POST["validarUsuario"];
    $valUsuario ->ajaxValidarUsuario();
}